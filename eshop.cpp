#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector>
#include<conio.h>
using namespace std;

class Proion{
private:	
	string model,katask,foto,perigr;
	float timi;
	int kodikos,posotita;
public:
    void set_kodikos(int kod){kodikos=kod;}
	void set_model(string mo){model=mo;}	
	void set_katask(string kat){katask=kat;}	
	void set_foto(string f){foto=f;}	
	void set_perigr(string pe){perigr=pe;}	
    void set_timi(float tim){timi=tim;}
    void set_posotita(int po){posotita=po;}
	int get_posotita(){return posotita;}	
	int get_kodikos(){return kodikos;}
	string get_model(){return model;}
	string get_katask(){return katask;}
	string get_foto(){return foto;}
	string get_perigr(){return perigr;}
	float get_timi(){return timi;}	
};


class Ypologistes: public Proion{
private:
	float ram,cpu,megethos_diskou;
	string eidos_diskou,karta_grafikon;
	int diathesima;
public:
	Ypologistes(){}
	Ypologistes(string strpc) {//Contructor gia diavasma apo arxeio
		istringstream issin(strpc);
		string split[13];
		int i = 0;

		while (getline(issin, split[i], '|')) {
			i++;
		}

		this->ram = stof(split[0]);
		this->cpu = stof(split[1]);
		this->megethos_diskou = stof(split[2]);
		this->eidos_diskou = split[3];
		this->karta_grafikon = split[4];
		this->diathesima = stoi(split[5]);
		this->set_model(split[6]);
		this->set_katask(split[7]);
		this->set_foto(split[8]);
		this->set_perigr(split[9]);
		this->set_timi(stof(split[10]));
		this->set_kodikos(stoi(split[11]));
		this->set_posotita(stoi(split[12]));
	}
	void set_ram(float ra){ram=ra;}
	void set_cpu(float cp){cpu=cp;}
	void set_megethos_diskou(float md){megethos_diskou=md;}
	void set_eidos_diskou(string ed){eidos_diskou=ed;}
	void set_karta_grafikon(string kg){karta_grafikon=kg;}
	void set_diathesima(int dia){diathesima=dia;}
    int get_diathesima(){return diathesima;}
	float get_ram(){return ram;}
	float get_cpu(){return cpu;}
	float get_megethos_diskou(){return megethos_diskou;}
    string get_eidos_diskou(){return eidos_diskou;}
	string get_karta_grafikon(){return karta_grafikon;}
	void save(ofstream &outfile) {outfile << this->toString() << endl;}//methodos gia grapsimo se arxeio
	string toString() {//paragogi tou katalilou string gia to grapsimo
		string str = to_string(ram) + "|" + to_string(cpu) + "|" + to_string(megethos_diskou) + "|" + eidos_diskou + "|" + karta_grafikon + "|" + to_string(diathesima) + "|" + this->get_model() + "|" + this->get_katask() + "|" + this->get_foto() + "|" + this->get_perigr() + "|" + to_string(this->get_timi())+ "|" + to_string(this->get_kodikos()) + "|" + to_string(this->get_posotita());
		return str;
	}
};

class Smartphones: public Proion{
private:
  int eggr_video_4d;	
  float megethos_othonis,diarkia_batarias;
  int diathesima;
public:
  Smartphones(){}
  Smartphones(string strsp) {//Contructor gia diavasma apo arxeio
	  istringstream issin(strsp);
	  string split[11];
	  int i = 0;

	  while (getline(issin, split[i], '|')) {
		  i++;
	  }

	  this->eggr_video_4d = stoi(split[0]);
	  this->megethos_othonis = stof(split[1]);
	  this->diarkia_batarias = stof(split[2]);
	  this->diathesima = stoi(split[3]);
	  this->set_model(split[4]);
	  this->set_katask(split[5]);
	  this->set_foto(split[6]);
	  this->set_perigr(split[7]);
	  this->set_timi(stof(split[8]));
	  this->set_kodikos(stoi(split[9]));
	  this->set_posotita(stoi(split[10]));
  }
  void set_eggr_video_4d(int ev4){eggr_video_4d=ev4;}
  void set_megethos_othonis(float mego){megethos_othonis=mego;}
  void set_diarkia_batarias(float db){diarkia_batarias=db;}
  void set_diathesima(int dia){diathesima=dia;}
  int get_diathesima(){return diathesima;}
  int get_eggr_video_4d(){return eggr_video_4d;}
  float get_megethos_othonis(){return megethos_othonis;}
  float get_diarkia_batarias(){return diarkia_batarias;}
  void save(ofstream &outfile) { outfile << this->toString() << endl; }//methodos gia grapsimo se arxeio
  string toString() {//paragogi tou katalillou sting gia grapsimo se axeio
	  string str = to_string(eggr_video_4d) + "|" + to_string(megethos_othonis) + "|" + to_string(diarkia_batarias) + "|" +  to_string(diathesima) + "|" + this->get_model() + "|" + this->get_katask() + "|" + this->get_foto() + "|" + this->get_perigr() + "|" + to_string(this->get_timi()) + "|" + to_string(this->get_kodikos()) + "|" + to_string(this->get_posotita());
	  return str;
  }
};

class Tileoraseis: public Proion{
private:	
  float diagonios;
  string katask_etairia;
  int trisd_prov;
  int diathesima;
public:
  Tileoraseis(){}
  Tileoraseis(string strtv) {//Contructor gia diavasma apo arxeio
	  istringstream issin(strtv);
	  string split[11];
	  int i = 0;

	  while (getline(issin, split[i], '|')) {
		  i++;
	  }

	  this->diagonios = stof(split[0]);
	  this->katask_etairia = split[1];
	  this->trisd_prov = stoi(split[2]);
	  this->diathesima = stoi(split[3]);
	  this->set_model(split[4]);
	  this->set_katask(split[5]);
	  this->set_foto(split[6]);
	  this->set_perigr(split[7]);
	  this->set_timi(stof(split[8]));
	  this->set_kodikos(stoi(split[9]));
	  this->set_posotita(stoi(split[10]));
  }
  void set_diagonios(float diag){diagonios=diag;}
  void set_katask_etairia(string ke){katask_etairia=ke;}
  void set_trisd_prov(int tp){trisd_prov=tp;}
  void set_diathesima(int dia){diathesima=dia;}
  int get_diathesima(){return diathesima;}
  float get_diagonios(){return diagonios;}
  string get_katask_etairia(){return katask_etairia;}
  int get_trisd_prov(){return trisd_prov;}
  void save(ofstream &outfile) { outfile << this->toString() << endl; }//methodos gia grapsimo se arxeio
  string toString() {//paragogi tou katalilou string gia grapsimo se arxeio
	  string str = to_string(diagonios) + "|" + katask_etairia + "|" + to_string(trisd_prov) + "|" + to_string(diathesima) + "|" + this->get_model() + "|" + this->get_katask() + "|" + this->get_foto() + "|" + this->get_perigr() + "|" + to_string(this->get_timi()) + "|" + to_string(this->get_kodikos()) + "|" + to_string(this->get_posotita());
	  return str;
  }

};

class Kalathi{
private:
	vector<Ypologistes> pcs;
	vector<Smartphones> sps;
	vector<Tileoraseis> tvs;
public:
	Kalathi(){}
	void add_Ypologistes(Ypologistes yp){
	pcs.push_back(Ypologistes());
    pcs[pcs.size()-1]=yp;}
	vector<Ypologistes>get_Ypologistes(){return pcs;}
	void clear_Ypologistes(){pcs.clear();}//hrisimopoihtai gia katharismo kalathiou
	void add_Ypologistes_vector(vector<Ypologistes> pcc){pcs=pcc;}
	
	void add_Smartphones(Smartphones sm){
	sps.push_back(Smartphones());
    sps[sps.size()-1]=sm;}
	vector<Smartphones>get_Smartphones(){return sps;}
	void clear_Smartphones(){sps.clear();}//hrisimopoihtai gia katharismo kalathiou
	void add_Smartphones_vector(vector<Smartphones> spp){sps=spp;}
	
	void add_Tileoraseis(Tileoraseis ti){
	tvs.push_back(Tileoraseis());
    tvs[tvs.size()-1]=ti;}
	vector<Tileoraseis>get_Tileoraseis(){return tvs;}
	void clear_Tileoraseis(){tvs.clear();}//hrisimopoihtai gia katharismo kalathiou
	void add_Tileoraseis_vector(vector<Tileoraseis> tvv){tvs=tvv;}
};

class Hristes{
private:
	string hristis,kodikos;
public:
	void set_hristis(string hri){hristis=hri;}
	void set_kodikos(string kod){kodikos=kod;}
	string get_hristis(){return hristis;}
	string get_kodikos(){return kodikos;}
};

class Diaheiristis: public Hristes{
public: 
    Diaheiristis(){}
	Diaheiristis(string stradm) {//Consttructor gia diavasma apo arxeio
		istringstream issin(stradm);
		string split[2];
		int i = 0;

		while (getline(issin, split[i], '|')) {
			i++;
		}

		this->set_hristis(split[0]);
		this->set_kodikos(split[1]);
		
	}
	void save(ofstream &outfile) { outfile << this->toString() << endl; }//methodos gia grapsimo se arxeio
	string toString() {//paragogi tou katalilou string gia grapsimo se arxeio
		string str = this->get_hristis() + "|" + this->get_kodikos();
		return str;
	}
};

class Pelates: public Hristes{
private:
   string afm,thl,dieuthinsi;
public:	
   void set_afm(string af){afm=af;}
   void set_thl(string th){thl=th;}
   void set_dieuthinsi(string di){dieuthinsi=di;}
   string get_afm(){return afm;}
   string get_thl(){return thl;}
   string get_dieuthinsi(){return dieuthinsi;}
};

class Pelatologio: public Pelates{
private:
   vector<Pelates> pelatologio;	
public:
   Pelatologio(){}	
   void add_Pelates(Pelates ps){pelatologio.push_back(ps);}
   vector<Pelates> get_Pelates(){return pelatologio;}   
};

class Fysika_Prosopa: public Pelates{
private:
	string onoma,epitheto,at;
public:
   Fysika_Prosopa(){}
   Fysika_Prosopa(string strfp) {
	   istringstream issin(strfp);
	   string split[8];
	   int i = 0;

	   while (getline(issin, split[i], '|')) {
		   i++;
	   }

	   this->onoma = split[0];
	   this->epitheto= split[1];
	   this->at = split[2];
	   this->set_afm(split[3]);
	   this->set_thl(split[4]);
	   this->set_dieuthinsi(split[5]);
	   this->set_hristis(split[6]);
	   this->set_kodikos(split[7]);
	   
   }
   void set_onoma(string on){onoma=on;}
   void set_epitheto(string ep){epitheto=ep;}
   void set_at(string a){at=a;}
   string get_onoma(){return onoma;}
   string get_epitheto(){return epitheto;}
   string get_at(){return at;}
   void save(ofstream &outfile) { outfile << this->toString() << endl; }
   string toString() {
	   string str = onoma + "|" + epitheto + "|" + at + "|" + this->get_afm() + "|" + this->get_thl() + "|" + this->get_dieuthinsi() + "|" + this->get_hristis() + "|" + this->get_kodikos();
	   return str;
   }
};

class Nomika_Prosopa: public Pelates{
private:
	string eponimia,ipeuthinos_epiki,fax;
	float ekptosi;//h ekptosi pairnei timi apo 0 eos 1 kai pollaplasiazete me tin timi tou kathe proiontos
	//p.x. ena proion kostizei 800 kai ehei ekptosi 50%, ara ektposi=0.5 kai teliki timi=800*0.5=400.
public:
   Nomika_Prosopa(){}
   Nomika_Prosopa(string strnp) {
	   istringstream issin(strnp);
	   string split[9];
	   int i = 0;

	   while (getline(issin, split[i], '|')) {
		   i++;
	   }

	   this->eponimia = split[0];
	   this->ipeuthinos_epiki = split[1];
	   this->fax = split[2];
	   this->ekptosi = stof(split[3]);
	   this->set_afm(split[4]);
	   this->set_thl(split[5]);
	   this->set_dieuthinsi(split[6]);
	   this->set_hristis(split[7]);
	   this->set_kodikos(split[8]);
   }
   void set_eponimia(string epo){eponimia=epo;}
   void set_ipeuthinos_epiki(string ie){ipeuthinos_epiki=ie;}
   void set_fax(string fa){fax=fa;}
   void set_ekptosi(float ekp){ekptosi=ekp;}
   float get_ekptosi(){return ekptosi;}
   string get_eponimia(){return eponimia;}
   string get_ipeuthinos_epiki(){return ipeuthinos_epiki;}
   string get_fax(){return fax;}
   void save(ofstream &outfile) { outfile << this->toString() << endl; }
   string toString() {
	   string str = eponimia + "|" + ipeuthinos_epiki + "|" + fax + "|" + to_string(ekptosi)+ "|" + this->get_afm() + "|" + this->get_thl() + "|" + this->get_dieuthinsi() + "|" + this->get_hristis() + "|" + this->get_kodikos();
	   return str;
   }
};
 class Paragelies{
private:
  string pelatis,katastasi_par;
  int kodikos_par;
  float teliko_kostos;
  string proionta=" ";//ola ta proionta me tis posotites pou einai stin paragelia
public:
  Paragelies(){}
  Paragelies(string strpar) {
	  istringstream issin(strpar);
	  string split[5];
	  int i = 0;

	  while (getline(issin, split[i], '|')) {
		  i++;
	  }

	  this->pelatis = split[0];
	  this->katastasi_par = split[1];
	  this->kodikos_par = stoi(split[2]);
	  this->teliko_kostos = stof(split[3]);
	  this->proionta = split[4];
  }
  void set_proion(string mo, int pos) {//kanei string ola ta proionta me tis posotites pou einai stin paragelia
	  proionta = proionta + "Proion: " + mo + " Posotita: " +to_string(pos) + ", ";
  }
  void set_kodikos_par(int kp){kodikos_par=kp;}
  void set_pelatis(string pel){pelatis=pel;}
  void set_katastasi_par(string kp){katastasi_par=kp;}
  void set_teliko_kostos(float tk){teliko_kostos=tk;}
  string get_paragelies() { return proionta; }
  int get_kodikos_par(){return kodikos_par;}
  string get_pelatis(){return pelatis;}
  string get_katastasi_par(){return katastasi_par;}
  float get_teliko_kostos(){return teliko_kostos;}
  void save(ofstream &outfile) { outfile << this->toString() << endl; }
  string toString() {
	  string str = pelatis + "|" + katastasi_par + "|" + to_string(kodikos_par) + "|" + to_string(teliko_kostos) + "|" + proionta;
	  return str;
  }
 };

int main(){
	bool session=0;//gia session=0 tote den einai kapoios hristis syndemenos,gia session=1 tote einai kapoios hristis syndemenos
	string username,password;
	char logtype,select,select1;
	int hristis_i;//kata to login krataei tin thesi tou hristi ston vector pou ekane to login
	vector <Diaheiristis> admin;
	vector <Fysika_Prosopa> fp;
	vector <Nomika_Prosopa> np;
	vector <Ypologistes> pc;
	vector <Smartphones> sp;
	vector <Tileoraseis> tv;
	vector <Paragelies> par;
	Kalathi kfp,knp;//kfp=kalathi fysikou prosopou, knp=kalathi nomikou prosopou
	ifstream infile;
	ofstream outfile;
	string str_input;//string gia to diavasma apo arxeia

	//diavazo apo erxeio kai ta vazo se olous tous vectors
	infile.open("pc.dat");
	while (infile.good()) {
		getline(infile, str_input);
		if (str_input.length() > 1) {
			pc.push_back(Ypologistes(str_input));
		}}
	infile.close();
	infile.open("sp.dat");
	while (infile.good()) {
		getline(infile, str_input);
		if (str_input.length() > 1) {
			sp.push_back(Smartphones(str_input));
		}
	}
	infile.close();
	infile.open("tv.dat");
	while (infile.good()) {
		getline(infile, str_input);
		if (str_input.length() > 1) {
			tv.push_back(Tileoraseis(str_input));
		}
	}
	infile.close();
	infile.open("admin.dat");
	while (infile.good()) {
		getline(infile, str_input);
		if (str_input.length() > 1) {
			admin.push_back(Diaheiristis(str_input));
		}
	}
	infile.close();
	infile.open("fp.dat");
	while (infile.good()) {
		getline(infile, str_input);
		if (str_input.length() > 1) {
			fp.push_back(Fysika_Prosopa(str_input));
		}
	}
	infile.close();
	infile.open("np.dat");
	while (infile.good()) {
		getline(infile, str_input);
		if (str_input.length() > 1) {
			np.push_back(Nomika_Prosopa(str_input));
		}
	}
	infile.close();
	infile.open("par.dat");
	while (infile.good()) {
		getline(infile, str_input);
		if (str_input.length() > 1) {
			par.push_back(Paragelies(str_input));
		}
	}
	infile.close();

	
	while(1){//einai pio praktiko na trehei to programma sunexeia mehri na to klisei o hristis
		
	if(session==0){
	cout<<"Den eisai syndemenos"<<endl<<endl;	
	cout<<"1 Eisodos os diaheiristis"<<endl;
	cout<<"2 Eisodos os fysiko prosopo"<<endl;
	cout<<"3 Eisodos os nomiko prosopo"<<endl<<endl;
	
	do{
	logtype=_getch();	
	}while(logtype!='1' && logtype!='2' && logtype!='3');
	
	cout<<"Dose Username: ";
	cin>>username;
	cout<<"Dose Password: ";
	cin>>password;
	
	if(logtype=='1'){
	for(int i=0;i<admin.size();i++){//elenhos username kai password gia diaheiristes	
	if(username==admin[i].get_hristis() && password==admin[i].get_kodikos()){
		cout<<endl<<"Kalos irthes "<<admin[i].get_hristis()<<endl<<endl;
		session=1;
		hristis_i=i;
		break;
	}}}
	if(logtype=='2'){//elenhos username kai password gia fysika prosopa
	for(int i=0;i<fp.size();i++){	
	if(username==fp[i].get_hristis() && password==fp[i].get_kodikos()){
		cout<<"Kalos irthes "<<fp[i].get_hristis()<<endl<<endl;
		session=1;
		hristis_i=i;
		break;
	}}}
	if(logtype=='3'){//elenhos username kai password gia nomika prosopa
	for(int i=0;i<np.size();i++){	
	if(username==np[i].get_hristis() && password==np[i].get_kodikos()){
		cout<<"Kalos irthes "<<np[i].get_hristis()<<endl<<endl;
		session=1;
		hristis_i=i;
		break;
	}}}
	if(session==0){cout<<"Lathos username h password"<<endl<<endl;}	
    }else{
    	 if(logtype=='1'){//admin
    	 cout<<endl<<"1 Dhmiourgia logariasmou hristi"<<endl;
      	 cout<<"2 Ektyposi katalogou proionton"<<endl;
    	 cout<<"3 Prosthiki/Afairesi proionton apo to katastima"<<endl;
    	 cout<<"4 Enimerosi proiontos"<<endl;
    	 cout<<"5 Enimerosi katastasis paragelias"<<endl;
    	 cout<<"6 Ektyposi pelatologiou"<<endl;
    	 cout<<"7 Ektyposi paragelion ana pelati kai katastasi paragelias"<<endl;
    	 cout<<"8 Ypologismos kuklou ergasion"<<endl;
		 cout<<"9 Eksodos apo to programma" << endl;
    	 cout<<"0 Aposyndesi diaheiristi"<<endl<<endl;
		 select=_getch();
		 
		 switch(select){
		 case '1':{
		   cout<<"1 Dhmiourgia logariasmou typou diaheiristi"<<endl;
      	   cout<<"2 Dhmiourgia logariasmou typou fysikou prosopou"<<endl;
    	   cout<<"3 Dhmiourgia logariasmou typou nomikou prosopou"<<endl<<endl;
		   select1=_getch();
		    switch(select1){	
		    case '1':{
			string hriad,kodad;
			cout<<"Dose onoma hristi"<<endl;
			cin>>hriad;
			cout<<"Dose kodiko"<<endl;
			cin>>kodad;
			admin.push_back(Diaheiristis());
			admin[admin.size()-1].set_hristis(hriad);
	        admin[admin.size()-1].set_kodikos(kodad);
			outfile.open("admin.dat");//apothikeusi allagon tou vector
			for (int i = 0; i < admin.size(); i++)
			admin[i].save(outfile);
			outfile.close();
			break;}
			case '2':{
			string hrifp,kodfp,afmfp,thlfp,dieufp,onomafp,epithetofp,atfp;
			cout<<"Dose onoma hristi"<<endl;
			cin>>hrifp;
			cout<<"Dose kodiko"<<endl;
			cin>>kodfp;
			cout<<"Dose afm"<<endl;
			cin>>afmfp;
			cout<<"Dose thlefono"<<endl;
			cin>>thlfp;
			cout<<"Dose dieuthinsi"<<endl;
			cin>>dieufp;
			cout<<"Dose onoma"<<endl;
			cin>>onomafp;
			cout<<"Dose epitheto"<<endl;
			cin>>epithetofp;
			cout<<"Dose arithmo tautotitas"<<endl;
			cin>>atfp;
			fp.push_back(Fysika_Prosopa());
			fp[fp.size()-1].set_hristis(hrifp);
	        fp[fp.size()-1].set_kodikos(kodfp);
	        fp[fp.size()-1].set_afm(afmfp);
	        fp[fp.size()-1].set_thl(thlfp);
	        fp[fp.size()-1].set_dieuthinsi(dieufp);
	        fp[fp.size()-1].set_onoma(onomafp);
	        fp[fp.size()-1].set_epitheto(epithetofp);
	        fp[fp.size()-1].set_at(atfp);
			outfile.open("fp.dat");//apothikeusi allagon tou vector
			for (int i = 0; i < fp.size(); i++)
			fp[i].save(outfile);
			outfile.close();
			break;}
			case '3':{
			string hrinp,kodnp,afmnp,thlnp,dieunp,eponimianp,ipeuthinos_epikinp,faxnp;
			float ekptosinp;
			cout<<"Dose onoma hristi"<<endl;
			cin>>hrinp;
			cout<<"Dose kodiko"<<endl;
			cin>>kodnp;
			cout<<"Dose afm"<<endl;
			cin>>afmnp;
			cout<<"Dose thlefono"<<endl;
			cin>>thlnp;
			cout<<"Dose dieuthinsi"<<endl;
			cin>>dieunp;
			cout<<"Dose eponimia"<<endl;
			cin>>eponimianp;
			cout<<"Dose ipeuthino epikoinonias"<<endl;
			cin>>ipeuthinos_epikinp;
			cout<<"Dose fax"<<endl;
			cin>>faxnp;
			cout<<"Dose ekptosi"<<endl;
			cin>>ekptosinp;
			np.push_back(Nomika_Prosopa());
			np[np.size()-1].set_hristis(hrinp);
	        np[np.size()-1].set_kodikos(kodnp);
	        np[np.size()-1].set_afm(afmnp);
	        np[np.size()-1].set_thl(thlnp);
	        np[np.size()-1].set_dieuthinsi(dieunp);
	        np[np.size()-1].set_eponimia(eponimianp);
	        np[np.size()-1].set_ipeuthinos_epiki(ipeuthinos_epikinp);
	        np[np.size()-1].set_fax(faxnp);
	        np[np.size()-1].set_ekptosi(ekptosinp);
			outfile.open("np.dat");//apothikeusi allagon tou vector
			for (int i = 0; i < np.size(); i++)
			np[i].save(outfile);
			outfile.close();
			break;}	
	    	default:{cout<<"Lathos eisagogi"<<endl; break;}
		 }
		 break;
         }
		 case '2':{
		 for(int i=0;i<pc.size();i++){
		 if(pc[i].get_diathesima()>0){//an den yparhoun diathesima proionta den ta ektyponei	
		 cout<<pc[i].get_model()<<endl;	
		 cout<<"\t"<<"RAM: "<<pc[i].get_ram()<<endl;
	     cout<<"\t"<<"CPU: "<<pc[i].get_cpu()<<endl;
     	 cout<<"\t"<<"Megethos Diskou: "<<pc[i].get_megethos_diskou()<<endl;
	     cout<<"\t"<<"Eidos Diskou: "<<pc[i].get_eidos_diskou()<<endl;
	     cout<<"\t"<<"Karta Grafikon: "<<pc[i].get_karta_grafikon()<<endl;
	     cout<<"\t"<<"Diathesima: "<<pc[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<pc[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<pc[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<pc[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<pc[i].get_perigr()<<endl;
	     cout<<"\t"<<"Timi: "<<pc[i].get_timi()<<endl<<endl;
		 }}
		 for(int i=0;i<sp.size();i++){
		 if(sp[i].get_diathesima()>0){	//an den yparhoun diathesima proionta den ta ektyponei	
		 cout<<sp[i].get_model()<<endl;		
		 if(sp[i].get_eggr_video_4d())
		 cout<<"\t"<<"Dynatotita Eggrafis Video 4D: Nai"<<endl;
		 else
		 cout<<"\t"<<"Dynatotita Eggrafis Video 4D: Oxi"<<endl;	
   	     cout<<"\t"<<"Megethos Othonis: "<<sp[i].get_megethos_othonis()<<endl;
	     cout<<"\t"<<"Diarkeia Batarias: "<<sp[i].get_diarkia_batarias()<<endl;
	     cout<<"\t"<<"Diathesima: "<<sp[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<sp[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<sp[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<sp[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<sp[i].get_perigr()<<endl;
	     cout<<"\t"<<"Timi: "<<sp[i].get_timi()<<endl<<endl;
		 }}
		 for(int i=0;i<tv.size();i++){
		 if(tv[i].get_diathesima()>0){	//an den yparhoun diathesima proionta den ta ektyponei	
		 cout<<"TV "<<tv[i].get_katask_etairia()<<endl;	
		 if(tv[i].get_trisd_prov())
		 cout<<"\t"<<"3D Provoli: Nai"<<endl;
		 else
		 cout<<"\t"<<"3D Provoli: Oxi"<<endl;
		 cout<<"\t"<<"Diagonios: "<<tv[i].get_diagonios()<<endl;		
	     cout<<"\t"<<"Diathesima: "<<tv[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<tv[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<tv[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<tv[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<tv[i].get_perigr()<<endl;
	     cout<<"\t"<<"Timi: "<<tv[i].get_timi()<<endl<<endl;
		 }}
		 break;}
		 case '3':{
		 cout<<"1 Prosthiki Ypologisti"<<endl;
      	 cout<<"2 Prosthiki Smartphone"<<endl;
    	 cout<<"3 Prosthiki Tileoraseis"<<endl;
    	 cout<<"4 Afairesi Ypologisti"<<endl;
    	 cout<<"5 Afairesi Smartphone"<<endl;
    	 cout<<"6 Afairesi Tileoraseis"<<endl;
    	 select1=_getch();
    	 switch(select1){
		 case '1':{
		 	
		 string eidos_diskou,karta_grafikon,model,katask,foto,perigr;
		 float ram,cpu,megethos_diskou,timi;
		 int diathesima,kodikos;
		 
		 cout<<"Dose ram"<<endl;
		 cin>>ram;
		 cout<<"Dose cpu"<<endl;
		 cin>>cpu;
		 cout<<"Dose megethos diskou"<<endl;
		 cin>>megethos_diskou;
		 cout<<"Dose eidos diskou"<<endl;
		 cin>>eidos_diskou;
		 cout<<"Dose karta grafikon"<<endl;
		 cin>>karta_grafikon;
		 cout<<"Dose modelo"<<endl;
		 cin>>model;
		 cout<<"Dose kataskeuasti"<<endl;
		 cin>>katask;
		 cout<<"Dose fotografia"<<endl;
		 cin>>foto;
		 cout<<"Dose perigrafi"<<endl;
		 cin>>perigr;
		 cout<<"Dose timi"<<endl;
		 cin>>timi;
		 cout<<"Dose diathesimotita"<<endl;
		 cin>>diathesima;
		 cout<<"Dose kodiko"<<endl;
		 cin>>kodikos;
		 
		 pc.push_back(Ypologistes());
	     pc[pc.size()-1].set_ram(ram);
	     pc[pc.size()-1].set_cpu(cpu);
	     pc[pc.size()-1].set_megethos_diskou(megethos_diskou);
	     pc[pc.size()-1].set_eidos_diskou(eidos_diskou);
	     pc[pc.size()-1].set_karta_grafikon(karta_grafikon);
	     pc[pc.size()-1].set_diathesima(diathesima);
	     pc[pc.size()-1].set_kodikos(kodikos);
	     pc[pc.size()-1].set_model(model);
	     pc[pc.size()-1].set_katask(katask);
	     pc[pc.size()-1].set_foto(foto);
	     pc[pc.size()-1].set_perigr(perigr);
	     pc[pc.size()-1].set_timi(timi);
		 pc[pc.size() - 1].set_posotita(0);
		 outfile.open("pc.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < pc.size(); i++)
		 pc[i].save(outfile);
		 outfile.close();
		 break;}
		 case '2':{
		 string model,katask,foto,perigr;
		 float timi,megethos_othonis,diarkia_batarias;
		 int diathesima,kodikos;
		 int eggr_video_4d;
		 
		 cout<<"Dose megethos othonis"<<endl;
		 cin>>megethos_othonis;
		 cout<<"Dose diarkia batarias"<<endl;
		 cin>>diarkia_batarias;
		 cout<<"Dose dynatotita eggrafis video 4d (0 h 1)"<<endl;
		 cin>>eggr_video_4d;
		 cout<<"Dose modelo"<<endl;
		 cin>>model;
		 cout<<"Dose kataskeuasti"<<endl;
		 cin>>katask;
		 cout<<"Dose fotografia"<<endl;
		 cin>>foto;
		 cout<<"Dose perigrafi"<<endl;
		 cin>>perigr;
		 cout<<"Dose timi"<<endl;
		 cin>>timi;
		 cout<<"Dose diathesimotita"<<endl;
		 cin>>diathesima;
		 cout<<"Dose kodiko"<<endl;
		 cin>>kodikos;
		 
		 sp.push_back(Smartphones());
	     sp[sp.size()-1].set_megethos_othonis(megethos_othonis);
	     sp[sp.size()-1].set_diarkia_batarias(diarkia_batarias);
	     sp[sp.size()-1].set_eggr_video_4d(eggr_video_4d);
	     sp[sp.size()-1].set_diathesima(diathesima);
	     sp[sp.size()-1].set_kodikos(kodikos);
	     sp[sp.size()-1].set_model(model);
	     sp[sp.size()-1].set_katask(katask);
	     sp[sp.size()-1].set_foto(foto);
	     sp[sp.size()-1].set_perigr(perigr);
	     sp[sp.size()-1].set_timi(timi);
		 sp[sp.size() - 1].set_posotita(0);
		 outfile.open("sp.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < sp.size(); i++)
			 sp[i].save(outfile);
		 outfile.close();
		 break;}
		 case '3':{
		 string model,katask,foto,perigr,katask_etairia;
		 float timi,diagonios;
		 int diathesima,kodikos;
		 int trisd_prov;
		 
		 cout<<"Dose diagonio"<<endl;
		 cin>>diagonios;
		 cout<<"Dose 3D provoli (1 h 0)"<<endl;
		 cin>>trisd_prov;
		 cout<<"Dose kataskeuastria etairia"<<endl;
		 cin>>katask_etairia;
		 cout<<"Dose modelo"<<endl;
		 cin>>model;
		 cout<<"Dose kataskeuasti"<<endl;
		 cin>>katask;
		 cout<<"Dose fotografia"<<endl;
		 cin>>foto;
		 cout<<"Dose perigrafi"<<endl;
		 cin>>perigr;
		 cout<<"Dose timi"<<endl;
		 cin>>timi;
		 cout<<"Dose diathesimotita"<<endl;
		 cin>>diathesima;
		 cout<<"Dose kodiko"<<endl;
		 cin>>kodikos;
		 
		 tv.push_back(Tileoraseis());
		 tv[tv.size()-1].set_diagonios(diagonios);
		 tv[tv.size()-1].set_trisd_prov(trisd_prov);
		 tv[tv.size()-1].set_katask_etairia(katask_etairia);
		 tv[tv.size()-1].set_diathesima(diathesima);
	     tv[tv.size()-1].set_kodikos(kodikos);
	     tv[tv.size()-1].set_model(model);
	     tv[tv.size()-1].set_katask(katask);
	     tv[tv.size()-1].set_foto(foto);
	     tv[tv.size()-1].set_perigr(perigr);
	     tv[tv.size()-1].set_timi(timi);
		 tv[tv.size()-1].set_posotita(0);
		 outfile.open("tv.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < tv.size(); i++)
			 tv[i].save(outfile);
		 outfile.close();
		 break;}
		 case '4':{
		 int kodikosaf;
		 bool deleted=0;//metavliti gia to an diagraftike h ohi
		 cout<<"Dose ton kodiko proiontos tou Ypologisti"<<endl;
		 cin>>kodikosaf;	
		 for(int i=0;i<pc.size();i++){
		 	if(pc[i].get_kodikos()==kodikosaf){
		 	 pc.erase(pc.begin()+i);
		 	 deleted=1;
			 break;	
			}
	     }
	     if(deleted){cout<<"O Ypologistis afairethike epituhos"<<endl<<endl;}
	     else{cout<<"O Ypologistis den vrethike"<<endl<<endl;}
		 outfile.open("pc.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < pc.size(); i++)
			 pc[i].save(outfile);
		 outfile.close();
		 break;}
		 case '5':{
		 int kodikosaf;
		 bool deleted=0;
		 cout<<"Dose ton kodiko proiontos tou Smartphone"<<endl;
		 cin>>kodikosaf;	
		 for(int i=0;i<sp.size();i++){
		 	if(sp[i].get_kodikos()==kodikosaf){
		 	 sp.erase(sp.begin()+i);
		 	 deleted=1;
			 break;	
			}
	     }
	     if(deleted){cout<<"To Smartphone afairethike epituhos"<<endl<<endl;}
	     else{cout<<"To Smartphone den vrethike"<<endl<<endl;}
		 outfile.open("sp.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < sp.size(); i++)
			 sp[i].save(outfile);
		 outfile.close();
		 break;}
		 case '6':{
		 int kodikosaf;
		 bool deleted=0;
		 cout<<"Dose ton kodiko proiontos tis Tileoraseis"<<endl;
		 cin>>kodikosaf;	
		 for(int i=0;i<tv.size();i++){
		 	if(tv[i].get_kodikos()==kodikosaf){
		 	 tv.erase(tv.begin()+i);
		 	 deleted=1;
			 break;	
			}
	     }
	     if(deleted){cout<<"H Tileorasi afairethike epituhos"<<endl<<endl;}
	     else{cout<<"H Tileorasi den vrethike"<<endl<<endl;}
		 outfile.open("tv.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < tv.size(); i++)
			 tv[i].save(outfile);
		 outfile.close();
		 break;}
		 default:{cout<<"Lathos eisagogi"<<endl; break;}
		 }
		 break;}
		 case '4':{
		 cout<<"1 Enimerosi Ypologisti"<<endl;
      	 cout<<"2 Enimerosi Smartphone"<<endl;
    	 cout<<"3 Enimerosi Tileorasis"<<endl;
    	 select1=_getch();
    	 switch(select1){
    	 case '1':{
		 string eidos_diskou,karta_grafikon,model,katask,foto,perigr;
		 float ram,cpu,megethos_diskou,timi;
		 int diathesima,kodikos,thesi;
		 bool found=0;
		 
		 cout<<"Dose ton kodiko proiontos tou Ypologisti"<<endl;
		 cin>>kodikos;	
		 for(int i=0;i<pc.size();i++){
		 	if(pc[i].get_kodikos()==kodikos){
		 	 thesi=i;
		 	 found=1;
			 break;	
			}
	     }
		 if(found){
		 cout<<"O Ypologistis vrethike"<<endl<<endl;
		 cout<<"Dose ram"<<endl;
		 cin>>ram;
		 cout<<"Dose cpu"<<endl;
		 cin>>cpu;
		 cout<<"Dose megethos diskou"<<endl;
		 cin>>megethos_diskou;
		 cout<<"Dose eidos diskou"<<endl;
		 cin>>eidos_diskou;
		 cout<<"Dose karta grafikon"<<endl;
		 cin>>karta_grafikon;
		 cout<<"Dose modelo"<<endl;
		 cin>>model;
		 cout<<"Dose kataskeuasti"<<endl;
		 cin>>katask;
		 cout<<"Dose fotografia"<<endl;
		 cin>>foto;
		 cout<<"Dose perigrafi"<<endl;
		 cin>>perigr;
		 cout<<"Dose timi"<<endl;
		 cin>>timi;
		 cout<<"Dose diathesimotita"<<endl;
		 cin>>diathesima;
		 pc[thesi].set_ram(ram);
	     pc[thesi].set_cpu(cpu);
	     pc[thesi].set_megethos_diskou(megethos_diskou);
	     pc[thesi].set_eidos_diskou(eidos_diskou);
	     pc[thesi].set_karta_grafikon(karta_grafikon);
	     pc[thesi].set_diathesima(diathesima);
	     pc[thesi].set_model(model);
	     pc[thesi].set_katask(katask);
	     pc[thesi].set_foto(foto);
	     pc[thesi].set_perigr(perigr);
	     pc[thesi].set_timi(timi);
		 }else{cout<<"Lathos kodikos, o Ypoligistis den vrethike"<<endl<<endl;}
		 outfile.open("pc.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < pc.size(); i++)
			 pc[i].save(outfile);
		 outfile.close();
		 break;}
		 case '2':{
		 
		 string model,katask,foto,perigr;
		 float timi,megethos_othonis,diarkia_batarias;
		 int diathesima,kodikos,thesi;
		 int eggr_video_4d,found=0;
		 
		 cout<<"Dose ton kodiko proiontos tou Smartphone"<<endl;
		 cin>>kodikos;	
		 for(int i=0;i<sp.size();i++){
		 	if(sp[i].get_kodikos()==kodikos){
		 	 thesi=i;
		 	 found=1;
			 break;	
			}
	     }
		 
		 if(found){
		 cout<<"To Smartphone vrethike"<<endl<<endl;	
		 cout<<"Dose megethos othonis"<<endl;
		 cin>>megethos_othonis;
		 cout<<"Dose diarkia batarias"<<endl;
		 cin>>diarkia_batarias;
		 cout<<"Dose dynatotita eggrafis video 4d (boolean 0 h 1)"<<endl;
		 cin>>eggr_video_4d;
		 cout<<"Dose modelo"<<endl;
		 cin>>model;
		 cout<<"Dose kataskeuasti"<<endl;
		 cin>>katask;
		 cout<<"Dose fotografia"<<endl;
		 cin>>foto;
		 cout<<"Dose perigrafi"<<endl;
		 cin>>perigr;
		 cout<<"Dose timi"<<endl;
		 cin>>timi;
		 cout<<"Dose diathesimotita"<<endl;
		 cin>>diathesima;
		 
	     sp[thesi].set_megethos_othonis(megethos_othonis);
	     sp[thesi].set_diarkia_batarias(diarkia_batarias);
	     sp[thesi].set_eggr_video_4d(eggr_video_4d);
	     sp[thesi].set_diathesima(diathesima);
	     sp[thesi].set_model(model);
	     sp[thesi].set_katask(katask);
	     sp[thesi].set_foto(foto);
	     sp[thesi].set_perigr(perigr);
	     sp[thesi].set_timi(timi);
		 }else{cout<<"Lathos kodikos, to Smartphone den vrethike"<<endl<<endl;}
		 outfile.open("sp.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < sp.size(); i++)
			 sp[i].save(outfile);
		 outfile.close();
		 break;}
		 case '3':{
		 
		 string model,katask,foto,perigr,katask_etairia;
		 float timi,diagonios;
		 int diathesima,kodikos,thesi;
		 int trisd_prov,found=0;
		 
		 cout<<"Dose ton kodiko proiontos tis Tileorasis"<<endl;
		 cin>>kodikos;	
		 for(int i=0;i<tv.size();i++){
		 	if(tv[i].get_kodikos()==kodikos){
		 	 thesi=i;
		 	 found=1;
			 break;	
			}
	     }
		 
		 if(found){
		 cout<<"H Tileorasi vrethike"<<endl<<endl;
		 cout<<"Dose diagonio"<<endl;
		 cin>>diagonios;
		 cout<<"Dose 3D provoli (boolean 1 h 0)"<<endl;
		 cin>>trisd_prov;
		 cout<<"Dose kataskeuastria etairia"<<endl;
		 cin>>katask_etairia;
		 cout<<"Dose modelo"<<endl;
		 cin>>model;
		 cout<<"Dose kataskeuasti"<<endl;
		 cin>>katask;
		 cout<<"Dose fotografia"<<endl;
		 cin>>foto;
		 cout<<"Dose perigrafi"<<endl;
		 cin>>perigr;
		 cout<<"Dose timi"<<endl;
		 cin>>timi;
		 cout<<"Dose diathesimotita"<<endl;
		 cin>>diathesima;
		 
		 tv[thesi].set_diagonios(diagonios);
		 tv[thesi].set_trisd_prov(trisd_prov);
		 tv[thesi].set_katask_etairia(katask_etairia);
		 tv[thesi].set_diathesima(diathesima);
	     tv[thesi].set_model(model);
	     tv[thesi].set_katask(katask);
	     tv[thesi].set_foto(foto);
	     tv[thesi].set_perigr(perigr);
	     tv[thesi].set_timi(timi);
	     }else{cout<<"Lathos kodikos, h Tileorasi den vrethike"<<endl<<endl;}
		 outfile.open("tv.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < tv.size(); i++)
			 tv[i].save(outfile);
		 outfile.close();
		 break;}
		 default:{cout<<"Lathos eisagogi"<<endl; break;}		
		 }
		 break;}
		 case '5':{
		 string katastasi;	
		 int kodpar,thesi_par;
		 bool found=0;
		 cout<<"Dose kodiko paragelias"<<endl;
		 cin>>kodpar;
		 for(int i=0;i<par.size();i++){
		 if(par[i].get_kodikos_par()==kodpar){
		 	 found=1;
		 	 thesi_par=i;
			 }}
		 if(found){
		 cout<<"Dose tin katastasi tis paragelias"<<endl;
		 cin>>katastasi;
		 par[thesi_par].set_katastasi_par(katastasi);
		 }else{cout<<"Den vrethike paragelia me auton ton kodiko"<<endl<<endl;}
		 outfile.open("par.dat");
		 for (int i = 0; i < par.size(); i++)
			 par[i].save(outfile);
		 outfile.close();
		 break;}
		 case '6':{
		 cout<<"Fysika Prosopa:"<<endl<<"---------------------------"<<endl;	
		 for(int i=0;i<fp.size();i++){	
		  cout<<fp[i].get_hristis()<<endl;
	      }
	     cout<<endl<<"Nomika Prosopa:"<<endl<<"---------------------------"<<endl; 
	     for(int i=0;i<np.size();i++){	
		  cout<<np[i].get_hristis()<<endl;
	      } 
         cout<<endl;
		 break;}
		 case '7':{
		 if(par.size()>0){	
		 for(int i=0;i<fp.size();i++){
		   cout<<"Paragelies tou "<<fp[i].get_hristis()<<":"<<endl<<endl;
		   for(int j=0;j<par.size();j++){
		   	 if(fp[i].get_hristis()==par[j].get_pelatis()){
				 cout << "Kodikos Paragelias: " << par[j].get_kodikos_par() << endl;
			   cout<<"Katastasi Paragelias: "<<par[j].get_katastasi_par()<<endl;
			   cout <<"Proionta: " <<par[j].get_paragelies() << endl;
			   cout << "-------------------------------------------------------------" << endl;
			}}}
		 cout<<endl;	
		 for(int i=0;i<np.size();i++){
		   cout<<endl<<"Paragelies tou "<<np[i].get_hristis()<<":"<<endl<<endl;	
		   for(int j=0;j<par.size();j++){
		   	 if(np[i].get_hristis()==par[j].get_pelatis()){
				 cout << "Kodikos Paragelias: " << par[j].get_kodikos_par() << endl;
				 cout << "Katastasi Paragelias: " << par[j].get_katastasi_par() << endl;
				 cout << "Proionta: " << par[j].get_paragelies() << endl;
				 cout << "-------------------------------------------------------------" << endl;
			}}}	
		}
		 break;}
		 case '8':{
		 float esoda=0.0;	
		 for(int i=0;i<par.size();i++){
		  esoda+=par[i].get_teliko_kostos();
		 }	
		 cout<<"Plythos paragelion: "<<par.size()<<endl;
		 cout<<"Synolika esoda: "<<esoda<<endl;
		 break;}
		 case '9': {return 0; break;}
		 case '0':{session=0; break;}
		 default:{break;}
		 }
	     }
		 if(logtype=='2'){//fysiko prosopo
		 cout<<endl<<"1 Ektypwsh katalogou proiontwn"<<endl;
    	 cout<<"2 Prosthikh proiontos sto kalathi agorwn kai kathorismos posothtas"<<endl;
         cout<<"3 Ektypwsh periexomenwn kalathioy agoras"<<endl;
    	 cout<<"4 Diagrafh proiontos apo to kalathi agorwn"<<endl;
    	 cout<<"5 Dhmiourgia neas paraggelias"<<endl;
    	 cout<<"6 Provolh katastash paraggelias"<<endl;
		 cout<<"7 Eksodos apo to programma" << endl;
    	 cout<<"0 Aposyndesh pelath apo to systhma"<<endl<<endl;
		 select1=_getch();
		 switch(select1){
		 case '1':{
		 for(int i=0;i<pc.size();i++){
		 if(pc[i].get_diathesima()>0){	
		 cout<<pc[i].get_model()<<endl;	
		 cout<<"\t"<<"RAM: "<<pc[i].get_ram()<<endl;
	     cout<<"\t"<<"CPU: "<<pc[i].get_cpu()<<endl;
     	 cout<<"\t"<<"Megethos Diskou: "<<pc[i].get_megethos_diskou()<<endl;
	     cout<<"\t"<<"Eidos Diskou: "<<pc[i].get_eidos_diskou()<<endl;
	     cout<<"\t"<<"Karta Grafikon: "<<pc[i].get_karta_grafikon()<<endl;
	     cout<<"\t"<<"Diathesima: "<<pc[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<pc[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<pc[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<pc[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<pc[i].get_perigr()<<endl;
	     cout<<"\t"<<"Timi: "<<pc[i].get_timi()<<endl<<endl;
		 }}
		 for(int i=0;i<sp.size();i++){
		 if(sp[i].get_diathesima()>0){	
		 cout<<sp[i].get_model()<<endl;		
		 if(sp[i].get_eggr_video_4d())
		 cout<<"\t"<<"Dynatotita Eggrafis Video 4D: Nai"<<endl;
		 else
		 cout<<"\t"<<"Dynatotita Eggrafis Video 4D: Oxi"<<endl;	
   	     cout<<"\t"<<"Megethos Othonis: "<<sp[i].get_megethos_othonis()<<endl;
	     cout<<"\t"<<"Diarkeia Batarias: "<<sp[i].get_diarkia_batarias()<<endl;
	     cout<<"\t"<<"Diathesima: "<<sp[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<sp[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<sp[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<sp[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<sp[i].get_perigr()<<endl;
	     cout<<"\t"<<"Timi: "<<sp[i].get_timi()<<endl<<endl;
		 }}
		 for(int i=0;i<tv.size();i++){
		 if(tv[i].get_diathesima()>0){	
		 cout<<"TV "<<tv[i].get_katask_etairia()<<endl;	
		 if(tv[i].get_trisd_prov())
		 cout<<"\t"<<"3D Provoli: Nai"<<endl;
		 else
		 cout<<"\t"<<"3D Provoli: Oxi"<<endl;
		 cout<<"\t"<<"Diagonios: "<<tv[i].get_diagonios()<<endl;		
	     cout<<"\t"<<"Diathesima: "<<tv[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<tv[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<tv[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<tv[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<tv[i].get_perigr()<<endl;
	     cout<<"\t"<<"Timi: "<<tv[i].get_timi()<<endl<<endl;
		 }}
		 break;}	
		 case '2':{
		 int kodpro,thesipro,posot;
		 bool found=0;
		 cout<<"1 Prosthiki Ypologisti sto kalthi"<<endl;
		 cout<<"2 Prosthiki Smartphone sto kalthi"<<endl;
		 cout<<"3 Prosthiki Tileorasis sto kalthi"<<endl<<endl;
		 select1=_getch();
		 switch(select1){
		 case '1':{
		 cout<<"Dose kodiko proiontos tou Ypologisti"<<endl;
		 cin>>kodpro;
	
		 for(int i=0;i<pc.size();i++){
		 	if(pc[i].get_kodikos()==kodpro){
		 	 thesipro=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		  cout<<"Dose posotita"<<endl;
		  cin>>posot;	
		  if(pc[thesipro].get_diathesima()>=posot){
		   cout<<"O Ypologistis "<<pc[thesipro].get_model()<<" prosthethike sto kalathi"<<endl;
		   pc[thesipro].set_posotita(posot);
		   pc[thesipro].set_diathesima(pc[thesipro].get_diathesima()-posot);
		   kfp.add_Ypologistes(pc[thesipro]);
	       }else{ cout<<"Den yparhoun tosoi diathesimoi Ypologistes"<<endl;}
		 }else{cout<<"Lathos kodikos h den yparhei"<<endl;}	
		 outfile.open("pc.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < pc.size(); i++)
			 pc[i].save(outfile);
		 outfile.close();
		 break;}
		 case '2':{
		  cout<<"Dose kodiko proiontos tou Smartphone"<<endl;
		 cin>>kodpro;
	
		 for(int i=0;i<sp.size();i++){
		 	if(sp[i].get_kodikos()==kodpro){
		 	 thesipro=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		  cout<<"Dose posotita"<<endl;
		  cin>>posot;	
		  if(sp[thesipro].get_diathesima()>=posot){
		   cout<<"To Smartphone "<<sp[thesipro].get_model()<<" prosthethike sto kalathi"<<endl;
		   
		   sp[thesipro].set_posotita(posot);
		   sp[thesipro].set_diathesima(sp[thesipro].get_diathesima()-posot);
		   kfp.add_Smartphones(sp[thesipro]);
	       }else{ cout<<"Den yparhoun tosa diathesima Smartphones"<<endl;}
		 }else{cout<<"Lathos kodikos h den yparhei"<<endl;}
		 outfile.open("sp.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < sp.size(); i++)
			 sp[i].save(outfile);
		 outfile.close();
		 break;}
		 case '3':{
		 cout<<"Dose kodiko proiontos tis Tileorasis"<<endl;
		 cin>>kodpro;
	
		 for(int i=0;i<tv.size();i++){
		 	if(tv[i].get_kodikos()==kodpro){
		 	 thesipro=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		  cout<<"Dose posotita"<<endl;
		  cin>>posot;	
		  if(tv[thesipro].get_diathesima()>=posot){
		   cout<<"H Tileorasi "<<tv[thesipro].get_model()<<" prosthethike sto kalathi"<<endl;
		   tv[thesipro].set_posotita(posot);
		   tv[thesipro].set_diathesima(tv[thesipro].get_diathesima()-posot);
		   kfp.add_Tileoraseis(tv[thesipro]);
	       }else{ cout<<"Den yparhoun toses diathesimes Tileoraseis"<<endl;}
		 }else{cout<<"Lathos kodikos h den yparhei"<<endl;}
		 outfile.open("tv.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < tv.size(); i++)
			 tv[i].save(outfile);
		 outfile.close();
		 break;}
		 default:{break;}
		 }
		 break;}
		 case '3':{
		 vector <Ypologistes> pry;
		 vector <Smartphones> prs;
		 vector <Tileoraseis> prt;
		 int cnt=1;//emfanizei kata tin ektyposi tou proiontos tin seira pou bike
		 float teltim=0.0;
		 pry=kfp.get_Ypologistes();
		 prs=kfp.get_Smartphones();
		 prt=kfp.get_Tileoraseis();
		 cout<<endl<<"  Proion \t Posotita \t Timi"<<endl;
		 if(pry.size()>0){
		 for(int i=0;i<pry.size();i++){
		 cout<<cnt<<"  "<<pry[i].get_model()<<" \t "<<pry[i].get_posotita()<<" \t\t "<<pry[i].get_timi()*pry[i].get_posotita()<<endl;
		 cnt++;
		 teltim+=pry[i].get_timi()*pry[i].get_posotita();
		 }}
		 if(prs.size()>0){
		 for(int i=0;i<prs.size();i++){
		 cout<<cnt<<"  "<<prs[i].get_model()<<" \t "<<prs[i].get_posotita()<<" \t\t "<<prs[i].get_timi()*prs[i].get_posotita()<<endl;
		 cnt++;
		 teltim+=prs[i].get_timi()*prs[i].get_posotita();
		 }}
		 if(prt.size()>0){
		 for(int i=0;i<prt.size();i++){
		 cout<<cnt<<"  "<<prt[i].get_model()<<" \t "<<prt[i].get_posotita()<<" \t\t "<<prt[i].get_timi()*prt[i].get_posotita()<<endl;
		 cnt++;
		 teltim+=prt[i].get_timi()*prt[i].get_posotita();
		 }}
		 cout<<"      \t       \t Teliki timi: "<<teltim<<endl<<endl;
		 break;}
		 case '4':{
		 int thesiaf1,thesiaf2,kodikosaf;
		 bool found=0;	
		 cout<<"1 Afairesi Ypologisti apo to kalathi"<<endl;
		 cout<<"2 Afairesi Smartphone apo to kalathi"<<endl;
		 cout<<"3 Afairesi Tileorasis apo to kalathi"<<endl<<endl;
		 select1=_getch();
		 switch(select1){
		 case '1':{
		 vector <Ypologistes> ery;	
		 cout<<"Dose ton kodiko proiontos tou Ypologisti"<<endl;
		 cin>>kodikosaf;
		 ery=kfp.get_Ypologistes();	
		 for(int i=0;i<ery.size();i++){
		 	if(ery[i].get_kodikos()==kodikosaf){
		 	 thesiaf1=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		 for(int i=0;i<pc.size();i++){
		 	if(pc[i].get_kodikos()==kodikosaf){
		 	 thesiaf2=i;
			 break;	
			}}
		 pc[thesiaf2].set_diathesima(pc[thesiaf2].get_diathesima()+pc[thesiaf2].get_posotita());
		 pc[thesiaf2].set_posotita(0);
		 kfp.clear_Ypologistes();
		 ery.erase(ery.begin()+thesiaf1);
		 kfp.add_Ypologistes_vector(ery);
		 cout<<"O Ypologistis afairethike epituhos"<<endl<<endl;	
		 }else{cout<<"O Ypologistis den vrethike sto kalathi"<<endl<<endl;}
		 outfile.open("pc.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < pc.size(); i++)
			 pc[i].save(outfile);
		 outfile.close();
			break;
		 }	
		 case '2':{
		 vector <Smartphones> ers;	
		 cout<<"Dose ton kodiko proiontos tou Smartphone"<<endl;
		 cin>>kodikosaf;
		  ers=kfp.get_Smartphones();	
		 for(int i=0;i<ers.size();i++){
		 	if(ers[i].get_kodikos()==kodikosaf){
		 	 thesiaf1=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		 for(int i=0;i<sp.size();i++){
		 	if(sp[i].get_kodikos()==kodikosaf){
		 	 thesiaf2=i;
			 break;	
			}}
		 sp[thesiaf2].set_diathesima(sp[thesiaf2].get_diathesima()+sp[thesiaf2].get_posotita());
		 sp[thesiaf2].set_posotita(0);
		 kfp.clear_Smartphones();
		 ers.erase(ers.begin()+thesiaf1);
		 kfp.add_Smartphones_vector(ers);
		 cout<<"To Smartphone afairethike epituhos"<<endl<<endl;	
		 }else{cout<<"To Smartphone den vrethike sto kalathi"<<endl<<endl;}	
		 outfile.open("sp.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < sp.size(); i++)
			 sp[i].save(outfile);
		 outfile.close();
		 break;
		 }
         case '3':{
         vector <Tileoraseis> ert;	
		 cout<<"Dose ton kodiko proiontos tis Tileorasis"<<endl;
		 cin>>kodikosaf;
		 ert=kfp.get_Tileoraseis();	
		 for(int i=0;i<ert.size();i++){
		 	if(ert[i].get_kodikos()==kodikosaf){
		 	 thesiaf1=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		 for(int i=0;i<tv.size();i++){
		 	if(tv[i].get_kodikos()==kodikosaf){
		 	 thesiaf2=i;
			 break;	
			}}
		 tv[thesiaf2].set_diathesima(tv[thesiaf2].get_diathesima()+tv[thesiaf2].get_posotita());
		 tv[thesiaf2].set_posotita(0);
		 kfp.clear_Tileoraseis();
		 ert.erase(ert.begin()+thesiaf1);
		 kfp.add_Tileoraseis_vector(ert);
		 cout<<"H Tileorasi afairethike epituhos"<<endl<<endl;	
		 }else{cout<<"H Tileorasi den vrethike sto kalathi"<<endl<<endl;}
		 outfile.open("tv.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < tv.size(); i++)
			 tv[i].save(outfile);
		 outfile.close();
		 break;}
	     default:{break;}
	    }
		 break;}
		 case '5':{
		 vector <Ypologistes> parpc;
		 vector <Smartphones> parsp;
		 vector <Tileoraseis> partv;	
		 float tel_kost=0;
		 parpc=kfp.get_Ypologistes();
		 parsp=kfp.get_Smartphones();
		 partv=kfp.get_Tileoraseis();	
		 par.push_back(Paragelies());
		 par[par.size()-1].set_kodikos_par(par.size());
		 par[par.size()-1].set_pelatis(fp[hristis_i].get_hristis());
		 par[par.size()-1].set_katastasi_par("Sulogi proionton");
		 if (parpc.size()>0) {
			 for (int i = 0;i<parpc.size();i++) {
			 par[par.size() - 1].set_proion(parpc[i].get_model(), parpc[i].get_posotita());
			 }}
		 if (parsp.size()>0) {
			 for (int i = 0;i<parsp.size();i++) {
			 par[par.size() - 1].set_proion(parsp[i].get_model(), parsp[i].get_posotita());
			 }}
		 if (partv.size()>0) {
			 for (int i = 0;i<partv.size();i++) {
			 par[par.size() - 1].set_proion(partv[i].get_model(), partv[i].get_posotita());
			 }}
		  if(parpc.size()>0){
		 for(int i=0;i<parpc.size();i++){
		 tel_kost+=parpc[i].get_timi()*parpc[i].get_posotita();
		 }}
		 if(parsp.size()>0){
		 for(int i=0;i<parsp.size();i++){
		 tel_kost+=parsp[i].get_timi()*parsp[i].get_posotita();
		 }}
		 if(partv.size()>0){
		 for(int i=0;i<partv.size();i++){
		 tel_kost+=partv[i].get_timi()*partv[i].get_posotita();
		 }}
		 par[par.size()-1].set_teliko_kostos(tel_kost);
		 cout<<"H paragelia sas apothikeutike"<<endl;
		 cout<<"Arithmos paragelias: "<<par[par.size()-1].get_kodikos_par()<<endl;
		 cout<<"Synoliko kostos: "<<par[par.size()-1].get_teliko_kostos()<<endl;
		 kfp.clear_Ypologistes();//katharismos kalathiou
		 kfp.clear_Smartphones();
		 kfp.clear_Tileoraseis();
		 outfile.open("par.dat");//apothikeusi allagon tou vector
		 for (int i = 0; i < par.size(); i++)
			 par[i].save(outfile);
		 outfile.close();
		 break;}
		 case '6':{
		 int kodpar,thesi_par;
		 bool found=0,autos=0;
		 cout<<"Dose kodiko paragelias"<<endl;
		 cin>>kodpar;
		 for(int i=0;i<par.size();i++){
		 if(par[i].get_kodikos_par()==kodpar){
		 	if(par[i].get_pelatis()==fp[hristis_i].get_hristis()){
		 	 found=1;
		 	 autos=1;
		 	 thesi_par=i;
			 }}}
		 if(found){
		 	if(autos){
		 	 cout<<"Katastasi paragelias:  "<<par[thesi_par].get_katastasi_par()<<endl;	
		 }else{cout<<"Auti h paragelia einai allou hristi"<<endl<<endl;}
		 }else{cout<<"Den vrethike paragelia me auton ton kodiko"<<endl<<endl;}
		 break;}
		 case '7': {return 0; break;}
		 case '0':{
		 session=0; 
		 kfp.clear_Ypologistes();//katharismos kalathiou
		 kfp.clear_Smartphones();
		 kfp.clear_Tileoraseis(); 
		 break;}	
		 default:{break;}	
		 }
		 }
		 if(logtype=='3'){//nomiko prosopo
		 cout<<endl<<"1 Ektypwsh katalogou proiontwn"<<endl;
    	 cout<<"2 Prosthikh proiontos sto kalathi agorwn kai kathorismos posothtas"<<endl;
         cout<<"3 Ektypwsh periexomenwn kalathioy agoras"<<endl;
    	 cout<<"4 Diagrafh proiontos apo to kalathi agorwn"<<endl;
    	 cout<<"5 Dhmiourgia neas paraggelias"<<endl;
    	 cout<<"6 Provolh katastash paraggelias"<<endl;
		 cout<<"7 Eksodos apo to programma" << endl;
    	 cout<<"0 Eksodos kai apothikeusi"<<endl<<endl;    	 
		 select1=_getch();
		 switch(select1){
		 case '1':{
		 float teliki_timi;	
		 for (int i=0;i<pc.size();i++){
		 if(pc[i].get_diathesima()>0){	
		 cout<<pc[i].get_model()<<endl;	
		 cout<<"\t"<<"RAM: "<<pc[i].get_ram()<<endl;
	     cout<<"\t"<<"CPU: "<<pc[i].get_cpu()<<endl;
     	 cout<<"\t"<<"Megethos Diskou: "<<pc[i].get_megethos_diskou()<<endl;
	     cout<<"\t"<<"Eidos Diskou: "<<pc[i].get_eidos_diskou()<<endl;
	     cout<<"\t"<<"Karta Grafikon: "<<pc[i].get_karta_grafikon()<<endl;
	     cout<<"\t"<<"Diathesima: "<<pc[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<pc[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<pc[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<pc[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<pc[i].get_perigr()<<endl;
	     teliki_timi=(pc[i].get_timi())*(np[hristis_i].get_ekptosi());
	     cout<<"\t"<<"Timi: "<<teliki_timi<<endl<<endl;
		 }}
		 for(int i=0;i<sp.size();i++){
		 if(sp[i].get_diathesima()>0){	
		 cout<<sp[i].get_model()<<endl;		
		 if(sp[i].get_eggr_video_4d())
		 cout<<"\t"<<"Dynatotita Eggrafis Video 4D: Nai"<<endl;
		 else
		 cout<<"\t"<<"Dynatotita Eggrafis Video 4D: Oxi"<<endl;	
   	     cout<<"\t"<<"Megethos Othonis: "<<sp[i].get_megethos_othonis()<<endl;
	     cout<<"\t"<<"Diarkeia Batarias: "<<sp[i].get_diarkia_batarias()<<endl;
	     cout<<"\t"<<"Diathesima: "<<sp[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<sp[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<sp[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<sp[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<sp[i].get_perigr()<<endl;
	     teliki_timi=(sp[i].get_timi())*(np[hristis_i].get_ekptosi());
	     cout<<"\t"<<"Timi: "<<teliki_timi<<endl<<endl;
		 }}
		 for(int i=0;i<tv.size();i++){
		 if(tv[i].get_diathesima()>0){	
		 cout<<"TV "<<tv[i].get_katask_etairia()<<endl;	
		 if(tv[i].get_trisd_prov())
		 cout<<"\t"<<"3D Provoli: Nai"<<endl;
		 else
		 cout<<"\t"<<"3D Provoli: Oxi"<<endl;
		 cout<<"\t"<<"Diagonios: "<<tv[i].get_diagonios()<<endl;		
	     cout<<"\t"<<"Diathesima: "<<tv[i].get_diathesima()<<endl;
	     cout<<"\t"<<"Kodikos Proiontou: "<<tv[i].get_kodikos()<<endl;
	     cout<<"\t"<<"Kataskeuastis: "<<tv[i].get_katask()<<endl;
	     cout<<"\t"<<"URL Fotografias: "<<tv[i].get_foto()<<endl;
	     cout<<"\t"<<"Perigrafi: "<<tv[i].get_perigr()<<endl;
	     teliki_timi=(tv[i].get_timi())*(np[hristis_i].get_ekptosi());
	     cout<<"\t"<<"Timi: "<<teliki_timi<<endl<<endl;
		 }}
		 break;}	
		 case '2':{
		 int kodpro,thesipro,posot;
		 bool found=0;
		 cout<<"1 Prosthiki Ypologisti sto kalthi"<<endl;
		 cout<<"2 Prosthiki Smartphone sto kalthi"<<endl;
		 cout<<"3 Prosthiki Tileorasis sto kalthi"<<endl<<endl;
		 select1=_getch();
		 switch(select1){
		 case '1':{
		 cout<<"Dose kodiko proiontos tou Ypologisti"<<endl;
		 cin>>kodpro;
	
		 for(int i=0;i<pc.size();i++){
		 	if(pc[i].get_kodikos()==kodpro){
		 	 thesipro=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		  cout<<"Dose posotita"<<endl;
		  cin>>posot;	
		  if(pc[thesipro].get_diathesima()>=posot){
		   cout<<"O Ypologistis "<<pc[thesipro].get_model()<<" prosthethike sto kalathi"<<endl;
		   
		   pc[thesipro].set_posotita(posot);
		   pc[thesipro].set_diathesima(pc[thesipro].get_diathesima()-posot);
		   knp.add_Ypologistes(pc[thesipro]);
	       }else{ cout<<"Den yparhoun tosoi diathesimoi Ypologistes"<<endl;}
		 }else{cout<<"Lathos kodikos h den yparhei"<<endl;}	
		 outfile.open("pc.dat");
		 for (int i = 0; i < pc.size(); i++)
			 pc[i].save(outfile);
		 outfile.close();
		 break;}
		 case '2':{
		  cout<<"Dose kodiko proiontos tou Smartphone"<<endl;
		 cin>>kodpro;
	
		 for(int i=0;i<sp.size();i++){
		 	if(sp[i].get_kodikos()==kodpro){
		 	 thesipro=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		  cout<<"Dose posotita"<<endl;
		  cin>>posot;	
		  if(sp[thesipro].get_diathesima()>=posot){
		   cout<<"To Smartphone "<<sp[thesipro].get_model()<<" prosthethike sto kalathi"<<endl;
		   
		   sp[thesipro].set_posotita(posot);
		   sp[thesipro].set_diathesima(sp[thesipro].get_diathesima()-posot);
		   knp.add_Smartphones(sp[thesipro]);
	       }else{ cout<<"Den yparhoun tosa diathesima Smartphones"<<endl;}
		 }else{cout<<"Lathos kodikos h den yparhei"<<endl;}	
		 outfile.open("sp.dat");
		 for (int i = 0; i < sp.size(); i++)
			 sp[i].save(outfile);
		 outfile.close();
		 break;}
		 case '3':{
		 cout<<"Dose kodiko proiontos tis Tileorasis"<<endl;
		 cin>>kodpro;
	
		 for(int i=0;i<tv.size();i++){
		 	if(tv[i].get_kodikos()==kodpro){
		 	 thesipro=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		  cout<<"Dose posotita"<<endl;
		  cin>>posot;	
		  if(tv[thesipro].get_diathesima()>=posot){
		   cout<<"H Tileorasi "<<tv[thesipro].get_model()<<" prosthethike sto kalathi"<<endl;
		   
		   tv[thesipro].set_posotita(posot);
		   tv[thesipro].set_diathesima(tv[thesipro].get_diathesima()-posot);
		   knp.add_Tileoraseis(tv[thesipro]);
	       }else{ cout<<"Den yparhoun toses diathesimes Tileoraseis"<<endl;}
		 }else{cout<<"Lathos kodikos h den yparhei"<<endl;}	
		 outfile.open("tv.dat");
		 for (int i = 0; i < tv.size(); i++)
			 tv[i].save(outfile);
		 outfile.close();
		 break;}
		 default:{break;}
		 }
		 
		 break;}
		 case '3':{
		 vector <Ypologistes> pry;
		 vector <Smartphones> prs;
		 vector <Tileoraseis> prt;
		 int cnt=1;
		 float teltim=0.0;
		 pry=knp.get_Ypologistes();
		 prs=knp.get_Smartphones();
		 prt=knp.get_Tileoraseis();
		 cout<<endl<<"  Proion \t Posotita \t Timi"<<endl;
		 if(pry.size()>0){
		 for(int i=0;i<pry.size();i++){
		 cout<<cnt<<"  "<<pry[i].get_model()<<" \t "<<pry[i].get_posotita()<<" \t\t "<<pry[i].get_timi()*pry[i].get_posotita()*(np[hristis_i].get_ekptosi())<<endl;
		 cnt++;
		 teltim+=pry[i].get_timi()*pry[i].get_posotita()*(np[hristis_i].get_ekptosi());
		 }}
		 if(prs.size()>0){
		 for(int i=0;i<prs.size();i++){
		 cout<<cnt<<"  "<<prs[i].get_model()<<" \t "<<prs[i].get_posotita()<<" \t\t "<<prs[i].get_timi()*prs[i].get_posotita()*(np[hristis_i].get_ekptosi())<<endl;
		 cnt++;
		 teltim+=prs[i].get_timi()*prs[i].get_posotita()*(np[hristis_i].get_ekptosi());
		 }}
		 if(prt.size()>0){
		 for(int i=0;i<prt.size();i++){
		 cout<<cnt<<"  "<<prt[i].get_model()<<" \t "<<prt[i].get_posotita()<<" \t\t "<<prt[i].get_timi()*prt[i].get_posotita()*(np[hristis_i].get_ekptosi())<<endl;
		 cnt++;
		 teltim+=prt[i].get_timi()*prt[i].get_posotita()*(np[hristis_i].get_ekptosi());
		 }}
		 cout<<"      \t       \t Teliki timi: "<<teltim<<endl<<endl;
		 break;}
		 case '4':{
		 int thesiaf1,thesiaf2,kodikosaf;
		 bool found=0;	
		 
	
		 cout<<"1 Afairesi Ypologisti apo to kalathi"<<endl;
		 cout<<"2 Afairesi Smartphone apo to kalathi"<<endl;
		 cout<<"3 Afairesi Tileorasis apo to kalathi"<<endl<<endl;
		 select1=_getch();
		 
		 switch(select1){
		 case '1':{
		 vector <Ypologistes> ery;	
		 cout<<"Dose ton kodiko proiontos tou Ypologisti"<<endl;
		 cin>>kodikosaf;
		 ery=knp.get_Ypologistes();	
		 for(int i=0;i<ery.size();i++){
		 	if(ery[i].get_kodikos()==kodikosaf){
		 	 thesiaf1=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		 for(int i=0;i<pc.size();i++){
		 	if(pc[i].get_kodikos()==kodikosaf){
		 	 thesiaf2=i;
			 break;	
			}}
		 
		 pc[thesiaf2].set_diathesima(pc[thesiaf2].get_diathesima()+pc[thesiaf2].get_posotita());
		 pc[thesiaf2].set_posotita(0);
		 knp.clear_Ypologistes();
		 ery.erase(ery.begin()+thesiaf1);
		 knp.add_Ypologistes_vector(ery);
		
	
		 cout<<"O Ypologistis afairethike epituhos"<<endl<<endl;	
		 }else{cout<<"O Ypologistis den vrethike sto kalathi"<<endl<<endl;}	
		 outfile.open("pc.dat");
		 for (int i = 0; i < pc.size(); i++)
			 pc[i].save(outfile);
		 outfile.close();
			break;
		 }	
		 case '2':{
		 vector <Smartphones> ers;
		 cout<<"Dose ton kodiko proiontos tou Smartphone"<<endl;
		 cin>>kodikosaf;
		  ers=knp.get_Smartphones();	
		 for(int i=0;i<ers.size();i++){
		 	if(ers[i].get_kodikos()==kodikosaf){
		 	 thesiaf1=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		 for(int i=0;i<sp.size();i++){
		 	if(sp[i].get_kodikos()==kodikosaf){
		 	 thesiaf2=i;
			 break;	
			}}
		
		 sp[thesiaf2].set_diathesima(sp[thesiaf2].get_diathesima()+sp[thesiaf2].get_posotita());
		 sp[thesiaf2].set_posotita(0);
		 knp.clear_Smartphones();
		 ers.erase(ers.begin()+thesiaf1);
		 knp.add_Smartphones_vector(ers);
		
	
		 cout<<"To Smartphone afairethike epituhos"<<endl<<endl;	
		 }else{cout<<"To Smartphone den vrethike sto kalathi"<<endl<<endl;}	
		 outfile.open("sp.dat");
		 for (int i = 0; i < sp.size(); i++)
			 sp[i].save(outfile);
		 outfile.close();
		 break;
		 }
         case '3':{
         vector <Tileoraseis> ert;	
		 cout<<"Dose ton kodiko proiontos tis Tileorasis"<<endl;
		 cin>>kodikosaf;
		 ert=knp.get_Tileoraseis();	
		 for(int i=0;i<ert.size();i++){
		 	if(ert[i].get_kodikos()==kodikosaf){
		 	 thesiaf1=i;
		 	 found=1;
			 break;	
			}}
		 if(found){
		 for(int i=0;i<tv.size();i++){
		 	if(tv[i].get_kodikos()==kodikosaf){
		 	 thesiaf2=i;
			 break;	
			}}
		 
		 tv[thesiaf2].set_diathesima(tv[thesiaf2].get_diathesima()+tv[thesiaf2].get_posotita());
		 tv[thesiaf2].set_posotita(0);
		 knp.clear_Tileoraseis();
		 ert.erase(ert.begin()+thesiaf1);
		 knp.add_Tileoraseis_vector(ert);
		
	
		 cout<<"H Tileorasi afairethike epituhos"<<endl<<endl;	
		 }else{cout<<"H Tileorasi den vrethike sto kalathi"<<endl<<endl;}	
		 outfile.open("tv.dat");
		 for (int i = 0; i < tv.size(); i++)
			 tv[i].save(outfile);
		 outfile.close();
		 break;}
	     default:{break;}
	    }
		 break;}
		 case '5':{
		 vector <Ypologistes> parpc;
		 vector <Smartphones> parsp;
		 vector <Tileoraseis> partv;	
		 float tel_kost=0;
		 parpc=knp.get_Ypologistes();
		 parsp=knp.get_Smartphones();
		 partv=knp.get_Tileoraseis();	
		 par.push_back(Paragelies());
		 par[par.size()-1].set_kodikos_par(par.size());
		 par[par.size()-1].set_pelatis(np[hristis_i].get_hristis());
		 par[par.size()-1].set_katastasi_par("Sulogi proionton");
		 if (parpc.size()>0) {
			 for (int i = 0;i<parpc.size();i++) {
				 par[par.size() - 1].set_proion(parpc[i].get_model(), parpc[i].get_posotita());
			 }}
		 if (parsp.size()>0) {
			 for (int i = 0;i<parsp.size();i++) {
				 par[par.size() - 1].set_proion(parsp[i].get_model(), parsp[i].get_posotita());
			 }}
		 if (partv.size()>0) {
			 for (int i = 0;i<partv.size();i++) {
				 par[par.size() - 1].set_proion(partv[i].get_model(), partv[i].get_posotita());
			 }}
		  if(parpc.size()>0){
		 for(int i=0;i<parpc.size();i++){
		 tel_kost+=parpc[i].get_timi()*parpc[i].get_posotita()*(np[hristis_i].get_ekptosi());
		 }}
		 if(parsp.size()>0){
		 for(int i=0;i<parsp.size();i++){
		 tel_kost+=parsp[i].get_timi()*parsp[i].get_posotita()*(np[hristis_i].get_ekptosi());
		 }}
		 if(partv.size()>0){
		 for(int i=0;i<partv.size();i++){
		 tel_kost+=partv[i].get_timi()*partv[i].get_posotita()*(np[hristis_i].get_ekptosi());
		 }}
		 par[par.size()-1].set_teliko_kostos(tel_kost);
		 cout<<"H paragelia sas apothikeutike"<<endl;
		 cout<<"Arithmos paragelias: "<<par[par.size()-1].get_kodikos_par()<<endl;
		 cout<<"Synoliko kostos: "<<par[par.size()-1].get_teliko_kostos()<<endl;
		 knp.clear_Ypologistes();
		 knp.clear_Smartphones();
		 knp.clear_Tileoraseis();
		 break;}
		 case '6':{
		  int kodpar,thesi_par;
		 bool found=0,autos=0;
		 cout<<"Dose kodiko paragelias"<<endl;
		 cin>>kodpar;
		 for(int i=0;i<par.size();i++){
		 if(par[i].get_kodikos_par()==kodpar){
		 	if(par[i].get_pelatis()==np[hristis_i].get_hristis()){
		 	 found=1;
		 	 autos=1;
		 	 thesi_par=i;
			 }}}
		 if(found){
		 	if(autos){
		 	 cout<<"Katastasi paragelias:  "<<par[thesi_par].get_katastasi_par()<<endl;	
		 }else{cout<<"Auti h paragelia einai allou hristi"<<endl<<endl;}
		 }else{cout<<"Den vrethike paragelia me auton ton kodiko"<<endl<<endl;}
		 break;}
		 case '7': {return 0; break;}
		 case '0':{
		 session=0; 
		 knp.clear_Ypologistes();
		 knp.clear_Smartphones();
		 knp.clear_Tileoraseis();
		 break;}
		 default:{break;}	
		 }
	     }	
	}//telos if session
    }//telos while 
	system("pause");
	return 0;
}
